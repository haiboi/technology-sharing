﻿namespace SampleMiddleware;

public class SampleMiddleware
{
    /// <summary>
    /// 方法执行委托
    /// </summary>
    public delegate Task RequestDelegate(HttpSampleContext context);
}