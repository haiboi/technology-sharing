namespace SampleMiddleware;

public class HttpSampleContext
{
    /// <summary>
    /// 请求地址
    /// </summary>
    public string Path
    {
        get;
        set;
    }
}