using SampleMiddleware;

namespace ConsoleApp1;

public static class TestMiddlewarre
{
    public static void UseTestMiddleware(this ApplicationBuild applicationBuild)
    {
        applicationBuild.Use((next) =>
        {
            Console.WriteLine("use middleware 1 init");
            return (context) =>
            {   
                Console.WriteLine("use middleware 1 begin");
                next(context);
                Console.WriteLine("use middleware 1 end");
                return Task.CompletedTask;
            };
        });
        applicationBuild.Map("/str", (app) =>
        {
            app.Use((next) =>
            {
                return context =>
                {
                    Console.WriteLine("map middleware 1 begin");
                    next(context);
                    return Task.CompletedTask;
                };
            });
            app.Use((next) =>
            {
                return context =>
                {
                    Console.WriteLine("map middleware 2 begin");
                    next(context);
                    return Task.CompletedTask;
                };
            });
        });
        applicationBuild.Run((context =>
        {
            Console.WriteLine("run middleware end");
            return Task.CompletedTask;
        }));
        applicationBuild.Use((next) =>
        {
            Console.WriteLine("use middleware 2 init");
            return (context) =>
            {
                Console.WriteLine("use middleware 2 begin");
                next(context);
                Console.WriteLine("use middleware 2 end");
                return Task.CompletedTask;
            };
        });
    }
}