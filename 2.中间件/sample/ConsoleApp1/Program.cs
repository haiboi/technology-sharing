﻿// See https://aka.ms/new-console-template for more information

using System.Net;
using System.Net.Sockets;
using System.Text;
using ConsoleApp1;
using SampleMiddleware;

HttpListener httpListener = new HttpListener();
httpListener.Prefixes.Add("http://localhost:5102/");
httpListener.Start();
ApplicationBuild applicationBuild = new ApplicationBuild();
//注册一个测试中间件
applicationBuild.UseTestMiddleware();
//构建
var next=applicationBuild.Build();
while (httpListener.IsListening)
{
    var context =await httpListener.GetContextAsync();
    using var resp = context.Response;
    // resp.Headers.Set("Location","https://www.baidu.com");
    // resp.StatusCode = 307;
    resp.StatusCode = 200;
    await using Stream stream = resp.OutputStream;
    // await stream.WriteAsync(Encoding.Default.GetBytes("hello word"));
    await next.Invoke(new HttpSampleContext()
    {
        Path = context.Request.RawUrl
    });
}