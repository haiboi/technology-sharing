| 主题 | 作者 |时间|
| :-----:| :----: | :----: |
| 中间件 | Naruto | 20220922 |

1.原理分析

整个中间件实则是一个又一个的方法委托的调用链路,可以理解为装饰器模式

<img src="./img/1.jpg" alt="1" style="zoom:100%;" />

2.代码解析

​	2.1 每个中间件对应一个 Func<RequestDelegate,RequestDelegate> 委托 入参为下一个方法执行委托，出参为当前方法执行流程

​	2.2 核心方法**Use**（不管是Map,Run 还是其它的中间件的扩展注册方法都是最终调用的Use方法）

​	<img src="./img/use.jpg" alt="1" style="zoom:100%;" />

​	2.3 **Build 构建整个链路信息**



<img src="./img/build.jpg" alt="1" style="zoom:100%;" />	

3.demo