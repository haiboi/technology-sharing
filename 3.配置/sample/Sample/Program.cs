﻿// See https://aka.ms/new-console-template for more information

using Configuration;Console.WriteLine("Hello, Configuration!");

//构建测试内存配置信息
Dictionary<string, string> testData = new Dictionary<string, string>();
testData.Add("test:test1","1");
testData.Add("test:test1:test2","2");
testData.Add("test:test1:test2:test3","3");
//构建第二个配置 用于测试覆盖配置
Dictionary<string, string> testData2 = new Dictionary<string, string>();
testData2.Add("test:test1:test2:test3","new3");
//构建对象
IConfigurationBuilder builder= new ConfigurationBuilder();
builder.Add(new InMemoryConfigurationSource(testData))
    .Add(new InMemoryConfigurationSource(testData2));
IConfiguration configuration= builder.Build();
Console.WriteLine($"test:test1={configuration.Get("test:test1")}");
Console.WriteLine($"test:test1:test2={configuration.Get("test:test1:test2")}");
IConfiguration testSection=configuration.GetSection("test");
Console.WriteLine($"testSection:test:test1={testSection.Get("test1")}");
Console.WriteLine($"testSection:test:test1:test2={testSection.Get("test1:test2")}");
var testSection2=testSection.GetSection("test1");
Console.WriteLine($"testSection2:test:test1={testSection2.Get()}");
Console.WriteLine($"testSection2:test:test1:test2={testSection2.Get("test2")}");
Console.WriteLine($"testSection2:test:test1:test2:test3={testSection2.Get("test2:test3")}");

Console.ReadLine();