| 主题 | 作者 |时间|
| :-----:| :----: | :----: |
| 配置 | Naruto | 20221120 |

1.原理分析

[ConfigurationBuilder](https://github.com/dotnet/runtime/blob/main/src/libraries/Microsoft.Extensions.Configuration/src/ConfigurationBuilder.cs)

<img src="./img/runtime-core.jpg" alt="1" style="zoom:100%;" />

[ConfigurationRoot](https://github.com/dotnet/runtime/blob/main/src/libraries/Microsoft.Extensions.Configuration/src/ConfigurationRoot.cs)

<img src="./img/runtime-root.jpg" alt="1" style="zoom:100%;" />

<img src="./img/runtime-root-getconfiguration.jpg" alt="1" style="zoom:100%;" />


[WebHostBuilder](https://github.com/dotnet/aspnetcore/blob/main/src/Hosting/Hosting/src/WebHostBuilder.cs)

<img src="./img/aspnetcore-build.jpg" alt="1" style="zoom:100%;" />

[HostingHostBuilderExtensions](https://github.com/dotnet/runtime/blob/release/6.0/src/libraries/Microsoft.Extensions.Hosting/src/HostingHostBuilderExtensions.cs)

<img src="./img/runtime-configuration-default.jpg" alt="1" style="zoom:100%;" />

[EnvironmentVariablesConfigurationProvider](https://github.com/dotnet/runtime/blob/main/src/libraries/Microsoft.Extensions.Configuration.EnvironmentVariables/src/EnvironmentVariablesConfigurationProvider.cs)

<img src="./img/runtime-environment-replace.jpg" alt="1" style="zoom:100%;" />

2.代码解析

[IConfigurationProvider定义需要实现的配置数据访问的提供商](https://github.com/dotnet/runtime/blob/main/src/libraries/Microsoft.Extensions.Configuration.Abstractions/src/IConfigurationProvider.cs)

<img src="./img/runtime-provider.jpg" alt="1" style="zoom:100%;" />

[IConfigurationSource配置源](https://github.com/dotnet/runtime/blob/main/src/libraries/Microsoft.Extensions.Configuration.Abstractions/src/IConfigurationSource.cs)

<img src="./img/runtime-source.jpg" alt="1" style="zoom:100%;" />

2.1通过IConfigurationBuilder 接口将  IConfigurationSource 信息添加到集合中，然后调用Build方法将 IConfigurationSource中的 IConfigurationProvider 对象解析传递给 IConfiguration 构造函数，然后注入到容器中

2.2 获取数据本质上调用的都是IConfigurationProvider.TryGet，**读取的顺序是按照配置添加顺序的倒序**

<img src="./img/runtime-root-getconfiguration.jpg" alt="1" style="zoom:100%;" />

3.demo