﻿namespace Configuration;

/// <summary>
/// 配置的供应商提供者 所有的供应商 需要实现当前接口
/// </summary>
public interface IConfigurationProvider
{
    /// <summary>
    /// 提供配置的加载方法
    /// </summary>
    void Load();

    /// <summary>
    /// 获取值
    /// </summary>
    /// <param name="key"></param>
    /// <param name="str"></param>
    /// <returns></returns>
    bool TryGet(string key, out string str);

    /// <summary>
    /// 设置值
    /// </summary>
    /// <param name="key"></param>
    /// <param name="value"></param>
    void Set(string key, string value);
}