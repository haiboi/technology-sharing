namespace Configuration;

/// <summary>
/// 配置源信息，指向配置供应商
/// </summary>
public interface IConfigurationSource
{
    /// <summary>
    /// 构建供应商
    /// </summary>
    /// <returns></returns>
    IConfigurationProvider Build();
}