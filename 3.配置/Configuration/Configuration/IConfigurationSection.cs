namespace Configuration;

/// <summary>
/// 配置的部分节点信息
/// </summary>
public interface IConfigurationSection : IConfiguration
{
    /// <summary>
    /// 获取
    /// </summary>
    /// <returns></returns>
    string Get();
}