namespace Configuration;

/// <summary>
/// 配置构建
/// </summary>
public interface IConfigurationBuilder
{
    /// <summary>
    /// 添加配置
    /// </summary>
    /// <returns></returns>
    IConfigurationBuilder Add(IConfigurationSource source);
    /// <summary>
    /// 构建配置信息
    /// </summary>
    /// <returns></returns>
    IConfiguration Build();
}