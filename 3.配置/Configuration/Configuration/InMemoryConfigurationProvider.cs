namespace Configuration;

/// <summary>
/// 内存提供配置
/// </summary>
public class InMemoryConfigurationProvider : ConfigurationProviderAbstract
{
    public InMemoryConfigurationProvider(InMemoryConfigurationSource inMemoryConfiguration)
    {
        foreach (var item in inMemoryConfiguration.Data)
        {
            Data.Add(item.Key,item.Value);
        }
    }
}