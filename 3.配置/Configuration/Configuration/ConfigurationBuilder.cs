namespace Configuration;

public class ConfigurationBuilder : IConfigurationBuilder
{
    private readonly List<IConfigurationSource> _configurationSources;

    public ConfigurationBuilder()
    {
        _configurationSources = new List<IConfigurationSource>();
    }

    /// <summary>
    /// 添加配置源
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public IConfigurationBuilder Add(IConfigurationSource source)
    {
        _configurationSources.Add(source);
        return this;
    }

    /// <summary>
    /// 构建根节点
    /// </summary>
    /// <returns></returns>
    public IConfiguration Build()
    {
        var providers = new List<IConfigurationProvider>(_configurationSources.Count);
        providers.AddRange(_configurationSources.Select(item => item.Build()));
        return new ConfigurationRoot(providers);
    }
}