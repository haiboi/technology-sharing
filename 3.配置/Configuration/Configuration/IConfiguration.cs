namespace Configuration;

/// <summary>
/// 统一对外使用的配置接口 根配置
/// </summary>
public interface IConfiguration
{
    /// <summary>
    /// 获取
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    string Get(string key);

    /// <summary>
    /// 获取部分的配置信息
    /// </summary>
    /// <param name="sectionKey"></param>
    /// <returns></returns>
    IConfigurationSection GetSection(string sectionKey);
}