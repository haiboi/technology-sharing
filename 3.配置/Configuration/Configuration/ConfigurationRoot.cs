namespace Configuration;

public class ConfigurationRoot : IConfiguration
{
    private readonly List<IConfigurationProvider> _providers;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="providers"></param>
    public ConfigurationRoot(List<IConfigurationProvider> providers)
    {
        _providers = providers;
        foreach (var item in _providers)
        {
            item.Load();
        }
    }

    public string Get(string key)
    {
        //从最新的一个开始读取
        for (var i = _providers.Count-1; i >=0; i--)
        {
            var itemProvider = _providers[i];
            if (itemProvider.TryGet(key ,out var value))
            {
                return value;
            }
        }

        return null;
    }

    public IConfigurationSection GetSection(string sectionKey)
    {
       return new ConfigurationSection(this,sectionKey);
    }
}