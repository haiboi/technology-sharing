namespace Configuration;

public abstract class ConfigurationProviderAbstract : IConfigurationProvider
{
    protected Dictionary<string, string> Data { get; }

    protected ConfigurationProviderAbstract()
    {
        //忽略大小写
        Data = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
    }
    public virtual void Load()
    {
        
    }

    public virtual bool TryGet(string key, out string str) => Data.TryGetValue(key, out str);

    public virtual void Set(string key, string value) => Data[key] = value;
}