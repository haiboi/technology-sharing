namespace Configuration;

/// <summary>
/// 内存配置源提供者
/// </summary>
public class InMemoryConfigurationSource : IConfigurationSource
{
    /// <summary>
    /// 数据
    /// </summary>
    public Dictionary<string, string> Data { get; }

    /// <summary>
    /// 配置信息
    /// </summary>
    /// <param name="data"></param>
    public InMemoryConfigurationSource(Dictionary<string, string> data)
    {
        Data = data;
    }
    public IConfigurationProvider Build()
    {
        return new InMemoryConfigurationProvider(this);
    }
}