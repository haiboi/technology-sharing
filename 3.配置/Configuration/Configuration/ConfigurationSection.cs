namespace Configuration;

public class ConfigurationSection : IConfigurationSection
{
    private readonly IConfiguration _root;
    private readonly string _path;

    public ConfigurationSection(IConfiguration root, string path)
    {
        _root = root;
        _path = path;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public string Get(string key) => _root.Get(Combine(key));

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sectionKey"></param>
    /// <returns></returns>
    /// <exception cref="NotImplementedException"></exception>
    public IConfigurationSection GetSection(string sectionKey) => new ConfigurationSection(_root, Combine(sectionKey));

    public string Get()=> _root.Get(_path);

    /// <summary>
    /// 合并key
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    private string Combine(string key) => $"{_path}{ConfigurationConst.KeySegmen}{key}";
}