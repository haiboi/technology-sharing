﻿using Microsoft.Extensions.DependencyInjection;
using Model;
using System;
using System.Runtime.CompilerServices;

namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //构建容器对象的提供者接口
            var serviceCollection =
                //注册服务
                new ServiceCollection()
                  .AddTransient<ITestTransient, TestTransient>()
                  .AddSingleton<TestSingleton>()
                  .AddScoped<TestScope>()
                  ;
            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            var res = RuntimeFeature.IsDynamicCodeCompiled;
            for (int i = 0; i < 2; i++)
            {
                using (var scopeProvider = serviceProvider.CreateScope())
                {
                    //从容器获取服务
                    for (int j = 0; j < 2; j++)
                    {
                        scopeProvider.ServiceProvider.GetService<TestSingleton>();
                        scopeProvider.ServiceProvider.GetService<TestScope>();
                        scopeProvider.ServiceProvider.GetService<TestTransient>();
                    }
                }
            }
            //输出结果
            //TestSingleton
            //TestScope
            //TestTransient
            //TestTransient
            //TestScope
            //TestTransient
            //TestTransient
            Console.ReadKey();
        }
    }
}
