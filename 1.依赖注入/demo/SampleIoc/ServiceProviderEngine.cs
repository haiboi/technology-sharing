﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleIoc
{
    /// <summary>
    /// 
    /// </summary>
    public class ServiceProviderEngine : IDisposable
    {

        public ServiceProviderEngineScope EngineScope { get; set; }

        public ServiceProviderEngine(ServiceProviderEngineScope engineScope)
        {
            EngineScope = engineScope;
        }

        public ServiceProviderEngine(ServiceProviderEngineScope root, ServiceProviderEngineScope engineScope) : this(engineScope)
        {
            _root = root;
        }
        /// <summary>
        /// 
        /// </summary>
        private readonly ServiceProviderEngineScope _root;


        /// <summary>
        ///根容器
        /// </summary>
        public ServiceProviderEngineScope Root
        {
            get
            {
                if (_root == null)
                {
                    return EngineScope;
                }
                return _root;
            }
        }

        /// <summary>
        /// 获取服务
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            //验证对象的生命周期
            if (ServiceCache.GetLifeTime(serviceType) == Lifetime.Singleton)
            {
                return Root.GetService(serviceType);
            }
            return EngineScope.GetService(serviceType);
        }

        public void Dispose()
        {
            EngineScope?.Dispose();
        }
    }
}
