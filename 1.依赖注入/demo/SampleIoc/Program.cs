﻿using Microsoft.Extensions.DependencyInjection;
using Model;
using SampleIoc;
using System;

namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("SampleIoc!");
            //服务注册
            var service = new SampleIocServiceCollection()
                       .AddTransient<ITestTransient, TestTransient>()
                       .AddSingleton<ITestSingleton, TestSingleton>()
                       .AddScoped<ITestScope, TestScope>();
            //
            using (var serviceProvider = service
                     .BuildServiceProvider())
            {

                var testSingleton = serviceProvider.GetService(typeof(ITestSingleton)) as ITestSingleton;
                testSingleton.SayHello();
                var testSingleton2 = serviceProvider.GetService(typeof(ITestSingleton)) as ITestSingleton;
                testSingleton.SayHello();
                for (int i = 0; i < 10; i++)
                {
                    using (var scopeServiceProvider = serviceProvider.CreateScope())
                    {
                        var testSingleton3 = scopeServiceProvider.GetService(typeof(ITestSingleton)) as ITestSingleton;
                        testSingleton3.SayHello();

                        var ITestScope = scopeServiceProvider.GetService(typeof(ITestScope)) as ITestScope;
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
