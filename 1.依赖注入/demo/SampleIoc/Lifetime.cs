﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleIoc
{
    /// <summary>
    /// 生命周期
    /// </summary>
    public enum Lifetime
    {
        /// <summary>
        /// 
        /// </summary>
        Singleton,
        /// <summary>
        /// 
        /// </summary>
        Scoped,
        /// <summary>
        /// 
        /// </summary>
        Transient
    }
}
