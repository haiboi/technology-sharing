﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleIoc
{
    /// <summary>
    /// 服务注册信息缓存
    /// </summary>
    public class ServiceCache
    {
        private static IDictionary<Type, ServiceInfo> _cache = new Dictionary<Type, ServiceInfo>();
        /// <summary>
        /// 获取服务
        /// </summary>
        /// <param name="implType"></param>
        /// <returns></returns>
        public static ServiceInfo Get(Type implType)
        {
            if (_cache.TryGetValue(implType, out var serviceInfo))
            {
                return serviceInfo;
            }
            throw new InvalidOperationException("类型没有注册");
        }
        /// <summary>
        /// 获取生命周期
        /// </summary>
        /// <param name="implType"></param>
        /// <returns></returns>
        public static Lifetime GetLifeTime(Type implType)
        {
            var serviceInfo = Get(implType);
            return serviceInfo.Lifetime;
        }
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="serviceInfos"></param>
        internal static void Add(IList<ServiceInfo> serviceInfos)
        {
            foreach (var item in serviceInfos)
            {
                _cache.TryAdd(item.ImplType, item);
            }
        }
    }
}
