﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleIoc
{
    /// <summary>
    /// 服务信息
    /// </summary>
    public class ServiceInfo
    {
        /// <summary>
        /// 接口类型
        /// </summary>
        public Type ImplType { get; set; }
        /// <summary>
        /// 服务类型
        /// </summary>
        public Type ServiceType { get; set; }

        /// <summary>
        /// 生命周期
        /// </summary>
        public Lifetime Lifetime { get; set; }
    }
}
