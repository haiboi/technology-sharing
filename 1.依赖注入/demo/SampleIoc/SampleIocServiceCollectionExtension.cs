﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleIoc
{
    /// <summary>
    /// 
    /// </summary>
    public static class SampleIocServiceCollectionExtension
    {
        /// <summary>
        /// 注册单例
        /// </summary>
        /// <typeparam name="ImplType"></typeparam>
        /// <typeparam name="ServiceType"></typeparam>
        /// <param name="serviceCollection"></param>
        public static SampleIocServiceCollection AddSingleton<ImplType, ServiceType>(this SampleIocServiceCollection serviceCollection) where ServiceType : ImplType
        {
            serviceCollection.Add(new ServiceInfo()
            {
                ImplType = typeof(ImplType),
                ServiceType = typeof(ServiceType),
                Lifetime = Lifetime.Singleton
            });
            return serviceCollection;
        }
        /// <summary>
        /// 注册Scoped
        /// </summary>
        /// <typeparam name="ImplType"></typeparam>
        /// <typeparam name="ServiceType"></typeparam>
        /// <param name="serviceCollection"></param>
        public static SampleIocServiceCollection AddScoped<ImplType, ServiceType>(this SampleIocServiceCollection serviceCollection) where ServiceType : ImplType
        {
            serviceCollection.Add(new ServiceInfo()
            {
                ImplType = typeof(ImplType),
                ServiceType = typeof(ServiceType),
                Lifetime = Lifetime.Scoped
            });
            return serviceCollection;
        }
        /// <summary>
        /// 注册Transient
        /// </summary>
        /// <typeparam name="ImplType"></typeparam>
        /// <typeparam name="ServiceType"></typeparam>
        /// <param name="serviceCollection"></param>
        public static SampleIocServiceCollection AddTransient<ImplType, ServiceType>(this SampleIocServiceCollection serviceCollection) where ServiceType : ImplType
        {
            serviceCollection.Add(new ServiceInfo()
            {
                ImplType = typeof(ImplType),
                ServiceType = typeof(ServiceType),
                Lifetime = Lifetime.Transient
            });
            return serviceCollection;
        }

        /// <summary>
        /// 构建容器提供者
        /// </summary>
        /// <param name="serviceCollection"></param>
        /// <returns></returns>
        public static ISampleIocServiceProvider BuildServiceProvider(this SampleIocServiceCollection serviceCollection)
        {
            //初始化对象的生命周期信息
            ServiceCache.Add(serviceCollection._services);
            //创建一个根服务
            var root = new ServiceProviderEngineScope(serviceCollection._services);
            return new SampleIocServiceProvider(new ServiceProviderEngine(root), serviceCollection._services, true);
        }
    }
}
