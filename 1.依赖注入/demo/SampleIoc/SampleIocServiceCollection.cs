﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleIoc
{
    /// <summary>
    /// 服务集合信息
    /// </summary>
    public class SampleIocServiceCollection
    {
        internal readonly IList<ServiceInfo> _services;

        public SampleIocServiceCollection()
        {
            _services = new List<ServiceInfo>();
        }
        /// <summary>
        /// 添加服务信息
        /// </summary>
        /// <param name="serviceInfo"></param>
        public void Add(ServiceInfo serviceInfo)
        {
            _services.Add(serviceInfo);
        }
    }
}
