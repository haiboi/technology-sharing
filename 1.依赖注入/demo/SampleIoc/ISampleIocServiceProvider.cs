﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleIoc
{
    /// <summary>
    /// 容器提供者
    /// </summary>
    public interface ISampleIocServiceProvider : IDisposable
    {
        /// <summary>
        /// 获取服务
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        object GetService(Type serviceType);

        /// <summary>
        /// 创建一个新的作用域
        /// </summary>
        /// <returns></returns>
        ISampleIocServiceProvider CreateScope();
    }
}
