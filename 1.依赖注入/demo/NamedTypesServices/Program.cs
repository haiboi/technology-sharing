﻿using Microsoft.Extensions.DependencyInjection;
using Model;
using NamedTypesServices;
using System;
using System.Runtime.CompilerServices;

namespace demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            //构建容器对象的提供者接口
            IServiceProvider serviceProvider =
                //注册服务
                new ServiceCollection()
                  .AddTransient(MergeNamedType.Merge("test", typeof(TestTransient)),typeof(TestTransient))

                  .BuildServiceProvider();
            using (var scopeProvider = serviceProvider.CreateScope())
            {
                var service = scopeProvider.ServiceProvider.GetService(MergeNamedType.Get("test")) as TestTransient;
            }
            Console.ReadKey();
        }
    }
}
