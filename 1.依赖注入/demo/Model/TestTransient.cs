﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 瞬时对象测试
    /// </summary>
    public class TestTransient: ITestTransient
    {
        public TestTransient()
        {
            Console.WriteLine($"TestTransient");
        }

        public void Dispose()
        {
            Console.WriteLine($"TestTransient-Dispose");
        }
    }
}
