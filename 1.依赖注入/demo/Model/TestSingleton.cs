﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 单例对象
    /// </summary>
    public class TestSingleton : ITestSingleton
    {
        /// <summary>
        /// 对象的唯一id
        /// </summary>
        private Guid UniId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testScope"></param>
        public TestSingleton(ITestScope testScope)
        {
            UniId = Guid.NewGuid();
            Console.WriteLine($"TestSingleton");
        }

        public void Dispose()
        {
            Console.WriteLine($"TestSingleton-Dispose");
        }

        public void SayHello()
        {
            Console.WriteLine($"TestSingleton- HelloWord {UniId}");
        }
    }
}
