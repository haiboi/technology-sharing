﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 作用域范围测试
    /// </summary>
    public class TestScope : ITestScope
    {
        public TestScope(ITestTransient testTransient)
        {
            Console.WriteLine($"TestScope");
        }

        public void Dispose()
        {
            Console.WriteLine($"TestScope-Dispose");
        }
    }
}
