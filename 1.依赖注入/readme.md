| 主题 | 作者 |时间|
| :-----:| :----: | :----: |
| 依赖注入 | Naruto | 20210820 |
- [控制反转(IOC)](#控制反转ioc)
  - [控制反转的目的就是将控制权从应用程序转交到框架](#控制反转的目的就是将控制权从应用程序转交到框架)
- [依赖注入](#依赖注入)
  - [对象的注入方式](#对象的注入方式)
    - [构造函数注入](#构造函数注入)
    - [属性注入](#属性注入)
    - [方法注入](#方法注入)
  - [对象的生命周期(可以查看测试代码demo1)](#对象的生命周期可以查看测试代码demo1)
    - [单例](#单例)
    - [作用域](#作用域)
    - [瞬时](#瞬时)
    - [重要点](#重要点)
- [Microsoft.Extensions.DependencyInjection 源码分析依赖注入实现过程](#microsoftextensionsdependencyinjection-源码分析依赖注入实现过程)
  - [1. 服务注册](#1-服务注册)
  - [2. 服务注入](#2-服务注入)
- [实现一个简易的IOC](#实现一个简易的ioc)
# 控制反转(IOC)
## 控制反转的目的就是将控制权从应用程序转交到框架
> 1.    传统的软件设计，对象与对象之间的引用 通过如下图所示将会造成应用程序的耦合度很高，一旦某一个对象有问题，整个流程都会崩掉
<br/>

> ![传统模式](./img/tradition.png)

> 2.    控制反转目的就是在其中加个中间层，具体的对象引用管理由中间层来统一管理（这个中间层可以称其为框架层或者容器），这样对象与对象的引用可以转换成对象与框架层之间的应用，降低系统的耦合度
<br/>

> ![新模式](./img/now.png)
# 依赖注入
> 我们可以从上面的第二点知道，当我们使用了IOC之后对象与对象之间的引用，就转换成对应与框架（容器）之间的引用了，那么这个调用过程是如何实现的呢，这个时候我们就要引出<b>依赖注入</b>的模式了（当然实现这种模式还有其它的方式比如工厂，比如模板等），依赖注入模式将所有的对象信息都存储到一个容器中，而我们应用程序需要做的就是通过容器定义的规范（这个规范可以为接口，注解等方式）注册我们的对象信息，然后当我们想取对象的信息时，就可以让容器帮我们来获取执行的服务了
## 对象的注入方式
### 构造函数注入
>1. ASPNETCORE的默认支持模式也是唯一的注入方式，同时也是推荐的做法,请看下方的使用案例（伪代码）
>```c# 
>    public class TestServices
>    {
>       private readonly ITest _test;
>
>        public TestServices(ITest  test)
>        {
>            _test = test;
>        }
>    }
>```
### 属性注入
>2.1 通过定义属性的形式，从容器中获取服务（ASPNETCORE默认不支持）,可使用第三方包例如Autofac
### 方法注入
>3.1 ASPNETCORE默认仅支持Startup中的Configure方法支持
>```c#
>        public void Configure(IApplicationBuilder app, IWebHostEnvironment env){
>
>        }
>```
>3.2 ASPNETCORE中也可以在控制器层通过方法参数注解的方式实现方式注入
>```c#
>        //通过特性FromServices来注入服务
>        public async Task Test([FromServices]ITest test)
>        {
>
>        }
>```
## 对象的生命周期(可以查看测试代码demo1)
### 单例
>1. 对象的缓存是存储到容器的根对象缓存下面，所以全局唯一
### 作用域
>1. scope生命周期每次都是创建一个新的容器的提供者，在当前范围中创建的scope对象信息都是维护在每个scope容器的缓存中，所以在同一个scope范围中获取的对象一样，整个ASPNETCORE请求都会创建一个新的scope范围容器
### 瞬时
>1. 每个获取的都是一个新的对象(无缓存)

<br/>

![cemo1](./img/demo1.png)
### 重要点
>1. 单例模式对象不能注入Scope对象和Transient对象，因为单例对象全局唯一不会释放掉，这样也会间接导致其引用的对象得不到释放，从而对象升级为单例对象

# Microsoft.Extensions.DependencyInjection 源码分析依赖注入实现过程
<!-- ## 1. ASPNETCORE支持两种实现方式一种通过emit一种是表达式[源码地址](https://source.dot.net/#Microsoft.Extensions.DependencyInjection/ServiceLookup/CompiledServiceProviderEngine.cs,ceaa78e72cb54382) -->
## 1. 服务注册
>1 整个ASPNETCORE所有的服务注册信息都使用[ServiceCollection](https://source.dot.net/#Microsoft.Extensions.DependencyInjection.Abstractions/ServiceCollection.cs,beaaadffb389924e)注册服务到我们的容器中

>2 然后使用[ServiceDescriptor](https://source.dot.net/#Microsoft.Extensions.DependencyInjection.Abstractions/ServiceDescriptor.cs,b593fd2837338f2a)对象信息，将我们的注册信息维护到集合中,对外可通过调用<b>AddTransient</b>,<b>AddSingleton</b>,<b>AddScoped</b>,<b>Add</b>方式添加,以下为对象部分源码
>```c#
>        //服务的生命周期 不填写默认为单例
>        public ServiceLifetime Lifetime { get; }
>        //如果使用接口注册的话，当前对象为接口的类型，否则与ImplementationType保持一致
>        public Type ServiceType { get; }
>        //服务的实现类型
>        [DynamicallyAccessedMembers(DynamicallyAccessedMemberTypes.PublicConstructors)]
>        public Type? ImplementationType { get; }
>```

>3 ServiceType 会作为缓存key将当前ServiceDescriptor对象存储到[字典](https://source.dot.net/#Microsoft.Extensions.DependencyInjection/ServiceLookup/CallSiteFactory.cs,b3200cd3834458b8)中，后续的服务注入通过此信息获取到具体的实现服务信息
>![CallSiteFactory](./img/CallSiteFactory.png)
>>3.1 扩展ServiceType，从上面一点可以看出ServiceType存在意思只是一个缓存key的作用，通过此字段信息获取对应的服务信息，就此我们可以使用此机制扩展一个命名类型的服务注册，<b>(代码在NamedTypesServices)</b>
>>![MergeNamedType](./img/MergeNamedType.png)
## 2. 服务注入
>1 IServiceProvider 对外开放的容器对象获取接口
>> 1 通过调用[BuildServiceProvider](https://source.dot.net/#Microsoft.Extensions.DependencyInjection/ServiceCollectionContainerBuilderExtensions.cs,d72a6ea7acdee50c)构建一个容器的服务提供者，然后保存此对象信息，服务的注入信息使用此接口获取

>> 2 系统构建的容器的同时，会默认将IServiceProvider和IServiceScopeFactory两个接口[注册](https://source.dot.net/#Microsoft.Extensions.DependencyInjection/ServiceLookup/ServiceProviderEngine.cs,afd54d529ed3bfb7)到我们的容器中
>>![ServiceProviderEngine](./img/ServiceProviderEngine.png)

>2 ServiceProviderEngine
>> 服务初始化系统的一些内置的服务，然后构建注入的服务

>3 [ILEmitResolverBuilder](https://source.dot.net/#Microsoft.Extensions.DependencyInjection/ServiceLookup/ILEmit/ILEmitResolverBuilder.cs,8d014f6291b31356)

>4 [ExpressionResolverBuilder](https://source.dot.net/#Microsoft.Extensions.DependencyInjection/ServiceLookup/Expressions/ExpressionResolverBuilder.cs,79c35dc7e8ec97f6)

>5 ServiceProviderEngineScope
>>整个依赖注入的内部核心对象 维持单例对象和scope对象的缓存，还有资源的释放
# 实现一个简易的IOC
>代码在demo项目SampleIoc