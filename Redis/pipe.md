### 前言

- ##### RESP3协议是一个简单的请求响应模型，在此基础上同时还支持[pipelining](https://redis.io/docs/manual/pipelining/)和[pub/sub](https://redis.io/docs/manual/pubsub/)两种特殊的模型

- ##### 代码案例使用[RedisNaruto](https://github.com/zhanghaiboshiwo/redis-naruto)客户端

#### 一.Pipelining（流水线）
- ```tex
    //请求响应模式 c 客户端 ，s 服务端
    c： INCR X
    s： 1
    c： INCR X
    s： 2
    c： INCR X
    s： 3
    c： INCR X
    s： 4
  ```

- ```te
  //流水线模式
  c： INCR X
  c： INCR X
  c： INCR X
  c： INCR X
  s： 1
  s： 2
  s： 3
  s： 4
  ```
  
- Redis 流水线是一种通过一次发出多个命令而无需等待对每个命令的响应来提高性能的技术，传统的请求响应模型每个命令都包含两个操作，发送命令，接收命令带来的回复（往返时间RTT），当客户端执行的命令过多的话，很容易造成性能瓶颈，Pipelining的实现就是为了在**同一个连接**中无需等待服务端的结果返回可以接着发送新的命令，等所有的消息发送完毕，最后在一次性读取消息的内容，这样的操作可以缩短整个RTT时间

- 当开启流水线的时候，服务端对命令的回复存储到内存中，所以如果使用流水线发送大量的命令的话，有可能会造成内存的暴涨，最好的结果是分批次发送


- ```c#
  //开启管道
  await using var pipe = await redisCommand.BeginPipeAsync();
  //发送命令 其中结果res1 res2 res3 res4 都是空值，因为结果需要使用EndPipeAsync读取
  var res1 = await pipe.IncrByAsync("x");
  var res2 = await pipe.IncrByAsync("x");
  var res3 = await pipe.IncrByAsync("x");
  var res4 = await pipe.IncrByAsync("x");
  //结束 一次性读取所有的消息，从管道中
  var res = await pipe.EndPipeAsync();
  //遍利所有的结果，res数组中的结果值顺序按照命令写入顺序进行返回
  foreach (var item in res)
  {
    _testOutputHelper.WriteLine(JsonConvert.SerializeObject(item));
  }
  ```
  
- 

#### 二.Pub/Sub（发布订阅）

-   Redis发布订阅，使用广播模式，当消息发送出去后，任何在线的客户端订阅的Topic匹配上此消息的话，都将收到一条消息，Redis保证消息最多只传递一次，并且不保证消息的可靠性，如果有较强的需求可以使用Redis5.0新的数据类型**Stream**

-   发布订阅的基本概念：
    - 发布者（Publisher）：向频道发布消息的客户端。
    - 订阅者（Subscriber）：订阅一个或多个频道，以接收发布者发送的消息。
    - 频道（Channel）：消息的传递通道，用于区分不同类型的消息。
    - 消息（Message）：发布者发送的信息，会被发送给所有订阅了相关频道的订阅者。

- 命令
```tex
PUBLISH ：发布者进行消息的发布

SUBSCRIBE：订阅者负责订阅一个或多个频道

UnSub：订阅者取消订阅，注意取消订阅的操作，需要使用和订阅同一个客户端连接
```
- SDK使用
```c#
//发布消息第一个参数是Channel 第二个参数是具体的消息内容
var res = await redisCommand.PublishAsync("test", "testtt");
```

```c#
//阻塞调用订阅方法
//订阅者在执行订阅操作时会被阻塞，直到有新的消息到达或取消订阅。因此，在进行订阅操作时，订阅者独享一个客户端连接来避免其阻塞
await redisCommand.SubscribeAsync(new[] {"test", "te"}, (topic, message) =>
                                  {
                                    _testOutputHelper.WriteLine($"topic={topic},message={message}");
                                    return Task.CompletedTask;
                                  });
```
 ```c#
 //取消订阅消息
 await redisCommand.UnSubscribeAsync(new[] {"test"});
 ```

- 注意事项
  - 持久化问题：Redis发布订阅模式不支持消息的持久化存储。如果订阅者在消息发布之前启动，它将无法接收到之前发布的消息，解决此问题可以使用Stream类型
  - Pub/Sub 与db无关，比如说在db0发布的消息，db1都能收到,如果有特殊的要求，可以在Channel前加上前缀隔离
