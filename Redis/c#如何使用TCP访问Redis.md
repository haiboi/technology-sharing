### Socket

- ##### 什么是Socket
  
  - Socket也叫套接字，同样也是文件描述符中的一种，它的存在允许同一机器上或者不同的机器上的两个进程之间进行数据传输，进程之间的数据传输协议分为TCP和UDP
  - Socket是操作系统提供的api，主要的方法，read,write,listen,connect,close (不同的操作系统提供的api名称可能不同)
- ##### Socket的应用场景
  
  - Socket大多使用在客户端和服务端的应用程序框架中（应用层）
    - FTP 文件传输协议
    - Http（超文本传输协议）用于从服务器传输超文本到本地浏览器的传送协议
    - SMTP 电子邮件传输的协议
    - DICOM 影像传输协议

### TCP/IP协议

- ##### TCP/IP协议栈分层

  - 链路层

    - 通过交换机、网卡（NIC）、网桥等设备来将数据帧从一个物理设备传输到另一个物理设备

  - IP层

    - 告诉我们数据在网络中如何传输到目标机器，处理网络传输中的路径选择的问题，选择去往目标机器的路径，然后将数据报发往适当的网络接口（数据传输的规则）。IPv4和IPv6是两个主要版本的IP协议。
    - IP层本身是面向消息的、不可靠的协议，如果发生数据丢失无法解决
    - IP层一次只能传递一个数据包
  
  - TCP/UDP传输层
  
    - TCP和UDP存在于IP层之上，决定了主机之间的数据传输方式
  
    - 什么是TCP
  
      - TCP是一种面向连接的、可靠的、基于字节流（可以理解为流水线生产）的传输层通信协议（套接字类型对应为SOCK_STREAM）, 同时TCP还负责流量控制，以避免网络拥塞，以下是关键特征
  
        - 传输的过程中数据不会丢失
        - 数据的传输是有序的
        - 传输的数据不存在边界
        - 套接字的连接需要一一对应
      - TCP中的分包，粘包，半包问题
  
        - ##### **分包：发送方一次性发送字符串”helloworld”，接收方却接收到了两个字符串”hello”和”world”。**
  
        - **粘包：发送方分别发送两个字符串”hello”+”world”，接收方却一次性接收到了”helloworld”。**
  
        - **半包：接收端在接收到的数据中只收到了一个完整数据包的部分内容，而不是整个数据包**
  
      - **如何处理包的边界问题？**
  
        - 固定大小，这种方法就是假定每一个包的大小都是固定字节数目，比如上文中讨论的每个包大小都是50个字节，接收端每收气50个字节就当成一个包
  
        - 指定包结束符，比如以一个\r\n(换行符和回车符)结束，这样对端只要收到这样的结束符，就可以认为收到了一个包，接下来的数据是下一个包的内容
  
        - 指定包的大小，这种方法结合了上述两种方法，一般包头是固定大小，包头中有一个字段指定包体或者整个大的大小，对端收到数据以后先解析包头中的字段得到包体或者整个包的大小，然后根据这个大小去界定数据的界线。
  
  
      - TCP的三次握手和4次挥手
      
        - 三次握手（在调用socket提供connect与服务端建立连接的时候）
      
        - 第一次握手(SYN=1, seq=x):
      
          客户端发送一个 TCP 的 SYN 标志位置1的包，指明客户端打算连接的服务器的端口，以及初始序号 X,保存在包头的序列号(Sequence Number)字段里。
      
          发送完毕后，客户端进入 `SYN_SEND` 状态。
      
        - 第二次握手(SYN=1, ACK=1, seq=y, ACKnum=x+1):
      
          服务器发回确认包(ACK)应答。即 SYN 标志位和 ACK 标志位均为1。服务器端选择自己 ISN 序列号，放到 Seq 域里，同时将确认序号(Acknowledgement Number)设置为客户的 ISN 加1，即X+1。 发送完毕后，服务器端进入 `SYN_RCVD` 状态。
      
        - 第三次握手(ACK=1，ACKnum=y+1)
      
          客户端再次发送确认包(ACK)，SYN 标志位为0，ACK 标志位为1，并且把服务器发来 ACK 的序号字段+1，放在确定字段中发送给对方，并且在数据段放写ISN的+1
      
          发送完毕后，客户端进入 `ESTABLISHED` 状态，当服务器端接收到这个包时，也进入 `ESTABLISHED` 状态，TCP 握手结束。
      
          ![结果](img/handshake.png)
      
      - 四次挥手（调用socket提供的close关闭连接的时候）
      
        - 第一次挥手(FIN=1，seq=x)
      
          假设客户端想要关闭连接，客户端发送一个 FIN 标志位置为1的包，表示自己已经没有数据可以发送了，但是仍然可以接受数据。
      
          发送完毕后，客户端进入 `FIN_WAIT_1` 状态。
      
        - 第二次挥手(ACK=1，ACKnum=x+1)
      
          服务器端确认客户端的 FIN 包，发送一个确认包，表明自己接受到了客户端关闭连接的请求，但还没有准备好关闭连接。
      
          发送完毕后，服务器端进入 `CLOSE_WAIT` 状态，客户端接收到这个确认包之后，进入 `FIN_WAIT_2` 状态，等待服务器端关闭连接。
      
        - 第三次挥手(FIN=1，seq=y)
      
          服务器端准备好关闭连接时，向客户端发送结束连接请求，FIN 置为1。
      
          发送完毕后，服务器端进入 `LAST_ACK` 状态，等待来自客户端的最后一个ACK。
      
        - 第四次挥手(ACK=1，ACKnum=y+1)
      
          客户端接收到来自服务器端的关闭请求，发送一个确认包，并进入 `TIME_WAIT`状态，等待可能出现的要求重传的 ACK 包。**（ps:看到这里我们就知道为什么.Net Core在2.1版本中推出了HttpClientFactory，因为传统的New Httpclient然后Dispose释放连接并不会立马进行连接的关闭，这就导致了高并发的请求的中会存在socket耗尽的问题，所以基于此问题，.Net Core2.1 推出了HttpClientFactory来实现连接的共用）**
      
          服务器端接收到这个确认包之后，关闭连接，进入 `CLOSED` 状态。
      
          客户端等待了某个固定时间（两个最大段生命周期，2MSL，2 Maximum Segment Lifetime）之后，没有收到服务器端的 ACK ，认为服务器端已经正常关闭连接，于是自己也关闭连接，进入 `CLOSED` 状态。
      
          ![结果](img/close.png)
      
      - TCP的数据交换
      
        - 通过ack消息确认机制来确保TCP消息的可靠性
      
          ![结果](img/dataexchange.png)
  
    - 什么是UDP
        - 提供面向消息的简单不可靠信息传送服务（套接字类型对应为SOCK_DGRAM）,关键特征如下
        - 消息不可靠，存在丢失风险
          - 消息没有顺序
        - 消息的传输速度快
          - 消息有边界感（数据的发送次数，和数据的接收次数是一致的）
          - 数据包存在大小限制
  
  - 应用层
  
    - 目的是为了屏蔽掉底层的细节，让程序员只需要关注如何利用socket进行编程，是选择TCP还是UDP作为传输方式进行数据交互，基于这些就产生了基于tcp或者udp的应用层应用协议比如 http，ftp，smtp等
  

### RESP协议

- ##### 什么是RESP协议（应用层协议规范）
  
  - 为了与 Redis 服务器进行通信，Redis 客户端使用称为 Redis 序列化协议 (RESP) 的协议，使用TCP进行连接
  
- ##### RESP2（Redis 2.0 版本中推出了RESP2版本）
  
  - 支持的数据类型
    - [Simple strings](https://redis.io/docs/reference/protocol-spec/#simple-strings)
    - [Simple Errors](https://redis.io/docs/reference/protocol-spec/#simple-errors)
    - [Integers](https://redis.io/docs/reference/protocol-spec/#integers)
    - [Bulk strings](https://redis.io/docs/reference/protocol-spec/#bulk-strings)
    - [Arrays](https://redis.io/docs/reference/protocol-spec/#arrays)
  
- ##### RESP3（Redis 6.0 版本中引入，7.0中完善）
  
  - 为什么协议会更新呢？
    - 由于在RESP2中redis支持的数据类型过少，没有足够的语义能力来允许客户端隐式地理解哪种转换是合适的
    - RESP 缺乏重要的数据类型：浮点数和布尔值分别作为字符串和整数返回。空值具有双重表示，称为*null chunk*和*null multi chunk*，这是无用的，因为区分空数组和空字符串的语义值不存在。
    - 没有办法返回二进制安全错误。当实现生成协议的通用 API 时，实现必须检查并从错误字符串中删除潜在的换行符
  - 增加的数据类型
    - [Nulls](https://redis.io/docs/reference/protocol-spec/#nulls)
    - [Booleans](https://redis.io/docs/reference/protocol-spec/#booleans)
    - [Doubles](https://redis.io/docs/reference/protocol-spec/#doubles)
    - [Big numbers](https://redis.io/docs/reference/protocol-spec/#big-numbers)
    - [Bulk errors](https://redis.io/docs/reference/protocol-spec/#bulk-errors) 二进制安全错误类型
    - [Verbatim strings](https://redis.io/docs/reference/protocol-spec/#verbatim-strings)
    - [Maps](https://redis.io/docs/reference/protocol-spec/#maps)
    - [Sets](https://redis.io/docs/reference/protocol-spec/#sets)
    - [Pushes](https://redis.io/docs/reference/protocol-spec/#pushes)

### c#访问Redis

- 使用Socket访问Redis （代码案例基于.net 6）

- ```c#
  // See https://aka.ms/new-console-template for more information
  
  using System.Buffers;
  using System.Net;
  using System.Net.Sockets;
  using System.Text;
  
  Console.WriteLine("Hello, RESP2!");
  
  TcpClient tcpClient = new TcpClient();
  var ipAddress = await Dns.GetHostAddressesAsync("127.0.0.1");
  await tcpClient.ConnectAsync(ipAddress, 55000);
  var messageTransport = new MessageTransport();
  //获取网络流
  var stream = tcpClient.GetStream();
  //发送授权消息
  await messageTransport.SendAsync(stream, new[]
  {
      "AUTH",
      "redispw"
  });
  //接收消息
  var authResult = await messageTransport.ReceiveSimpleMessageAsync(stream);
  //判断是否 成功
  if (authResult.IsEmpty())
  {
      Console.WriteLine("登陆失败");
  }
  
  //发送set命令
  await messageTransport.SendAsync(stream, new[]
  {
      "SET",
      "testKey",
      "testvalue"
  });
  _ = await messageTransport.ReceiveSimpleMessageAsync(stream);
  
  //查询值
  await messageTransport.SendAsync(stream, new[]
  {
      "GET",
      "testKey"
  });
  //获取结果
  var stringResult= await messageTransport.ReceiveSimpleMessageAsync(stream);
  if (!stringResult.IsEmpty())
  {
      Console.WriteLine($"获取get返回结果：testKey={stringResult}");
  }
  Console.ReadLine();
  
  /// <summary>
  /// 消息传输
  /// </summary>
  public sealed class MessageTransport
  {
      /// <summary>
      /// 换行
      /// </summary>
      private static readonly byte[] NewLine = Encoding.UTF8.GetBytes("\r\n");
  
      private static readonly byte CR = (byte) '\r';
  
  
      private static readonly byte LF = (byte) '\n';
  
      /// <summary>
      /// 发送消息
      /// </summary>
      /// <param name="stream"></param>
      /// <param name="args"></param>
      public async Task SendAsync(Stream stream, string[] args)
      {
          if (args is not {Length: > 0})
          {
              return;
          }
  
          await using var ms = new MemoryStream();
          ms.Position = 0;
          await ms.WriteAsync(Encoding.UTF8.GetBytes($"{RespMessage.ArrayString}{args.Length}"));
          await ms.WriteAsync(NewLine);
          //判断参数长度
          foreach (var item in args)
          {
              var argBytes = Encoding.UTF8.GetBytes(item);
              await ms.WriteAsync(Encoding.UTF8.GetBytes($"{RespMessage.BulkStrings}{argBytes.Length}"));
              await ms.WriteAsync(NewLine);
              await ms.WriteAsync(argBytes);
              await ms.WriteAsync(NewLine);
          }
  
          ms.Position = 0;
          await ms.CopyToAsync(stream);
      }
      
      /// <summary>
      /// 读取简易消息
      /// </summary>
      /// <param name="stream"></param>
      /// <returns></returns>
      /// <exception cref="NotSupportedException"></exception>
      public async Task<RedisValue> ReceiveSimpleMessageAsync(Stream stream)
      {
          //获取首位的 符号 判断消息回复类型
          var head = ReadFirstChar(stream);
          switch (head)
          {
              case RespMessage.SimpleString:
              case RespMessage.Number:
              {
                  return ReadLine(stream);
              }
              //数组
              case RespMessage.ArrayString:
              {
                  var length = ReadLine(stream);
                  if (length == -1)
                  {
                      return RedisValue.Null();
                  }
  
                  throw new NotSupportedException(nameof(RespMessage.ArrayString));
              }
              case RespMessage.BulkStrings:
              {
                  int offset = ReadLine(stream);
                  //如果为null
                  if (offset == -1)
                  {
                      return RedisValue.Null();
                  }
  
                  var result = await ReadBlobLineAsync(stream, offset);
  
                  return new RedisValue(result);
              }
              default:
              {
                  //错误
                  throw new Exception(ReadLine(stream).ToString());
              }
          }
      }
  
      /// <summary>
      /// 读取指定长度数据
      /// </summary>
      /// <param name="stream"></param>
      /// <param name="length">长度</param>
      /// <returns></returns>
      private static async Task<ReadOnlyMemory<byte>> ReadBlobLineAsync(Stream stream, int length)
      {
          //从内存池中租借
          await using var ms = new MemoryStream();
          ms.Position = 0;
          var totalLength = 0;
          while (true)
          {
              using (var memoryOwner = MemoryPool<byte>.Shared.Rent(length))
              {
                  var mem = await stream.ReadAsync(memoryOwner.Memory[..(length - totalLength)]);
                  await ms.WriteAsync(memoryOwner.Memory[..mem]);
                  totalLength += mem;
                  //读取 
                  //这里用大于等于 是因为访问其它情况导致 陷入死循环
                  if (totalLength >= length)
                  {
                      break;
                  }
              }
          }
  
          //读取换行信息
          await ReadCrlfAsync(stream);
          //获取真实的数据
          return new ReadOnlyMemory<byte>(ms.ToArray());
      }
  
      /// <summary>
      /// 读取换行
      /// </summary>
      /// <param name="stream"></param>
      private static async Task ReadCrlfAsync(Stream stream)
      {
          using var memoryOwner = MemoryPool<byte>.Shared.Rent(2);
          _ = await stream.ReadAsync(memoryOwner.Memory[..2]);
      }
  
      /// <summary>
      /// 读取第一行的字节信息
      /// </summary>
      /// <param name="stream"></param>
      /// <returns></returns>
      private static char ReadFirstChar(Stream stream)
      {
          var es = stream.ReadByte();
          //如果返回的-1 网络存在问题
          if (es == -1)
          {
              throw new IOException();
          }
  
          return (char) es;
      }
  
      /// <summary>
      /// 读取行数据
      /// </summary>
      /// <param name="stream"></param>
      /// <returns></returns>
      private static RedisValue ReadLine(Stream stream)
      {
          //
          using var ms =new MemoryStream();
          ms.Position = 0;
          while (true)
          {
              var msg = stream.ReadByte();
              if (msg < 0) break;
              //判断是否为换行 \r\n
              if (msg == CR)
              {
                  var msg2 = stream.ReadByte();
                  if (msg2 < 0) break;
                  if (msg2 == LF) break;
                  ms.WriteByte((byte) msg);
                  ms.WriteByte((byte) msg2);
              }
              else
                  ms.WriteByte((byte) msg);
          }
  
          return new RedisValue(ms.ToArray());
      }
  }
  
  /// <summary>
  /// 消息协议
  /// </summary>
  public static class RespMessage
  {
      /// <summary>
      /// 简单的字符串返回
      /// </summary>
      public const char SimpleString = '+';
  
      /// <summary>
      /// 错误
      /// </summary>
      public const char Error = '-';
  
      /// <summary>
      /// 数字
      /// </summary>
      public const char Number = ':';
  
      /// <summary>
      /// 批量字符串
      /// </summary>
      public const char BulkStrings = '$';
  
      /// <summary>
      /// 数组
      /// </summary>
      public const char ArrayString = '*';
  }
  
  /// <summary>
  /// redis返回值 包装器
  /// </summary>
  public readonly struct RedisValue
  {
      /// <summary>
      /// 回复的值
      /// </summary>
      private readonly ReadOnlyMemory<byte> _memory;
  
      public RedisValue(ReadOnlyMemory<byte> memory) : this(memory, false)
      {
      }
  
      public RedisValue(byte[] bytes)
      {
          _memory = new ReadOnlyMemory<byte>(bytes);
          IsError = false;
      }
  
      /// <summary>
      /// 
      /// </summary>
      /// <param name="memory"></param>
      /// <param name="isError"></param>
      private RedisValue(ReadOnlyMemory<byte> memory, bool isError)
      {
          _memory = memory;
          IsError = isError;
      }
  
      internal static RedisValue Null() => new();
  
      /// <summary>
      /// 错误消息
      /// </summary>
      /// <param name="memory"></param>
      /// <returns></returns>
      internal static RedisValue Error(ReadOnlyMemory<byte> memory) => new RedisValue(memory, true);
  
      /// <summary>
      /// 判断返回值是否为空
      /// </summary>
      /// <returns></returns>
      public bool IsEmpty() => _memory.IsEmpty;
  
      /// <summary>
      /// 是否错误
      /// </summary>
      public bool IsError { get; }
  
      /// <summary>
      /// 数据长度
      /// </summary>
      public long Length
      {
          get
          {
              if (_memory.IsEmpty)
              {
                  return 0;
              }
  
              return _memory.Length;
          }
      }
  
      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      public override string ToString() => Encoding.UTF8.GetString(_memory.Span);
  
      /// <summary>
      /// 转成字节
      /// </summary>
      public byte[] ToBytes => _memory.ToArray();
  
      /// <summary>
      /// 转成字节
      /// </summary>
      public ReadOnlyMemory<byte> ToReadOnlyMemory => _memory;
  
      private int ToInt()
      {
          return IsEmpty() ? 0 :int.Parse(ToString());
      }
  
      private long ToLong()
      {
          return IsEmpty() ? 0L : long.Parse(ToString()) ;
      }
  
      public static bool operator ==(RedisValue x, string y)
      {
          if (x.IsEmpty())
          {
              return false;
          }
  
          return x.ToString() == y;
      }
  
      public static bool operator !=(RedisValue x, string y)
      {
          if (x.IsEmpty())
          {
              return false;
          }
  
          return x.ToString() != y;
      }
      
      /// <summary>
      /// 
      /// </summary>
      /// <param name="value"></param>
      /// <returns></returns>
      public static implicit operator long(RedisValue value) => value.ToLong();
  
      /// <summary>
      /// 
      /// </summary>
      /// <param name="value"></param>
      /// <returns></returns>
      public static implicit operator int(RedisValue value) => value.ToInt();
  
      /// <summary>
      /// 
      /// </summary>
      /// <param name="value"></param>
      /// <returns></returns>
      public static implicit operator byte[](RedisValue value) => value.ToBytes;
  
      /// <summary>
      /// 
      /// </summary>
      /// <param name="value"></param>
      /// <returns></returns>
      public static implicit operator string(RedisValue value) => value.ToString();
  }
  ```
  
  ![结果](img/redisresult.png)

### 参考资料

- https://www.geeksforgeeks.org/tcp-connection-termination/?ref=lbp
- https://github.com/riba2534/TCP-IP-NetworkNote
- https://www.cyningsun.com/03-30-2023/network-transmission.html
- https://www.geeksforgeeks.org/tcp-3-way-handshake-process/?ref=lbp
- https://hit-alibaba.github.io/interview/basic/network/TCP.html
- TCP/IP网络编程 书籍
- https://redis.io/docs/reference/protocol-spec/
- https://github.com/redis/redis-specifications/tree/master/protocol

### 个人项目地址

- https://gitee.com/haiboi/redis-naruto

